//  Copyright © 2019 SWTecNN. All rights reserved.

import XCTest
@testable import QuestionsApp

class AuthenticationModuleAssemblyTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testAssemblyModuleForViewController() {

        //given
        let viewController = AuthenticationViewControllerMock()
        let assembly = AuthenticationAssembly()

        //when
        assembly.assemblyModuleForView(view: viewController)

        //then
        XCTAssertNotNil(viewController.output, "AuthenticationViewController is nil after assembly")
        XCTAssertTrue(viewController.output is AuthenticationPresenter, "output is not AuthenticationPresenter")

        let presenter: AuthenticationPresenter = viewController.output as! AuthenticationPresenter
        XCTAssertNotNil(presenter.view, "view in AuthenticationPresenter is nil after assembly")
        XCTAssertNotNil(presenter.router, "router in AuthenticationPresenter is nil after assembly")
        XCTAssertTrue(presenter.router is AuthenticationViewControllerMock, "router is not AuthenticationRouter")

        let interactor: AuthenticationInteractor = presenter.interactor as! AuthenticationInteractor
        XCTAssertNotNil(interactor.output, "output in AuthenticationInteractor is nil after assembly")
    }

    class AuthenticationViewControllerMock: AuthenticationViewController {
        
    }
}
