//  Copyright © 2019 SWTecNN. All rights reserved.

import XCTest
@testable import QuestionsApp

class DashboardInteractorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    class MockPresenter: IDashboardInteractorOutput {
        func socialEventObtained(_ socialEvent: SocialEvent) {
            
        }
        
        func socialEventObtainError(_ error: Error) {
            
        }
        
        func nameObtained(_ name: String) {
        }

        func nameObtainError(_ error: Error) {
        }

        func nameSaved() {
        }

        func nameSaveError(_ error: Error) {
        }

        func socialEventsObtained(_ socialEvents: [SocialEventModel]) {
        }

        func socialEventsObtainError(_ error: Error) {
        }
    }
}
