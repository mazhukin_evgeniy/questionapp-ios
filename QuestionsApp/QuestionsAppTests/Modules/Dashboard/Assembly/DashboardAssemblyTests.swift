//  Copyright © 2019 SWTecNN. All rights reserved.

import XCTest
@testable import QuestionsApp

class DashboardModuleAssemblyTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testAssemblyModuleForViewController() {

        //given
        let viewController = DashboardViewControllerMock()
        let assembly = DashboardAssembly()

        //when
        assembly.assemblyModuleForView(view: viewController)

        //then
        XCTAssertNotNil(viewController.output, "DashboardViewController is nil after assembly")
        XCTAssertTrue(viewController.output is DashboardPresenter, "output is not DashboardPresenter")

        let presenter: DashboardPresenter = viewController.output as! DashboardPresenter
        XCTAssertNotNil(presenter.view, "view in DashboardPresenter is nil after assembly")
        XCTAssertNotNil(presenter.router, "router in DashboardPresenter is nil after assembly")
        XCTAssertTrue(presenter.router is DashboardViewControllerMock, "router is not DashboardViewControllerMock")

        let interactor: DashboardInteractor = presenter.interactor as! DashboardInteractor
        XCTAssertNotNil(interactor.output, "output in DashboardInteractor is nil after assembly")
    }

    class DashboardViewControllerMock: DashboardViewController {
    }
}
