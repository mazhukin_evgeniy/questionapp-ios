//  Copyright © 2019 SWTecNN. All rights reserved.

import XCTest
@testable import QuestionsApp

class SocialEventInfoPresenterTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    class MockInteractor: ISocialEventInfoInteractor {
        func obtainSocialEvent(with id: String) {
        }
    }

    class MockRouter: ISocialEventInfoRouter {
        func openSocialEventQuestions(with id: String, role: Role) {
        }
        
        func openSocialEventMaterial(_ material: String) {
        }
    }

    class MockViewController: ISocialEventInfoView {
        
        func handleError(_ error: Error) {
        }
        
        func showProgress() {
        }
        
        func hideProgress() {
        }
        
        func showSocialEventInfo(_ socialEvent: SocialEvent) {
        }
        
        func enableQuestions() {
        }
        
        func setupInitialState() {

        }
    }
}
