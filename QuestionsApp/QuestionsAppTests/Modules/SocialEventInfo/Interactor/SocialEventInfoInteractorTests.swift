//  Copyright © 2019 SWTecNN. All rights reserved.

import XCTest
@testable import QuestionsApp

class SocialEventInfoInteractorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    class MockPresenter: ISocialEventInfoInteractorOutput {
        func socialEventObtained(_ socialEvent: SocialEvent) {
        }
        
        func socialEventObtainError(_ error: Error) {
        }
    }
}
