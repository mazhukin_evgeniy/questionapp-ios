//  Copyright © 2019 SWTecNN. All rights reserved.

import XCTest
@testable import QuestionsApp

class SocialEventInfoModuleAssemblyTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testAssemblyModuleForViewController() {

        //given
        let viewController = SocialEventInfoViewControllerMock()
        let assembly = SocialEventInfoAssembly()

        //when
        assembly.assemblyModuleForView(view: viewController)

        //then
        XCTAssertNotNil(viewController.output, "SocialEventInfoViewController is nil after assembly")
        XCTAssertTrue(viewController.output is SocialEventInfoPresenter, "output is not SocialEventInfoPresenter")

        let presenter: SocialEventInfoPresenter = viewController.output as! SocialEventInfoPresenter
        XCTAssertNotNil(presenter.view, "view in SocialEventInfoPresenter is nil after assembly")
        XCTAssertNotNil(presenter.router, "router in SocialEventInfoPresenter is nil after assembly")
        XCTAssertTrue(presenter.router is SocialEventInfoRouter, "router is not SocialEventInfoRouter")

        let interactor: SocialEventInfoInteractor = presenter.interactor as! SocialEventInfoInteractor
        XCTAssertNotNil(interactor.output, "output in SocialEventInfoInteractor is nil after assembly")
    }

    class SocialEventInfoViewControllerMock: SocialEventInfoViewController {
    }
}
