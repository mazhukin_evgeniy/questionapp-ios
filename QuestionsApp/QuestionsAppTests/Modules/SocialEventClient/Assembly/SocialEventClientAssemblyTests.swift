//  Copyright © 2019 SWTecNN. All rights reserved.

import XCTest
@testable import QuestionsApp

class SocialEventClientModuleAssemblyTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testAssemblyModuleForViewController() {

        //given
        let viewController = SocialEventClientViewControllerMock()
        let assembly = SocialEventClientAssembly()

        //when
        assembly.assemblyModuleForView(view: viewController)

        //then
        XCTAssertNotNil(viewController.clientOutput, "SocialEventClientViewController is nil after assembly")
        XCTAssertTrue(viewController.clientOutput is SocialEventClientPresenter, "output is not SocialEventClientPresenter")

        let presenter: SocialEventClientPresenter = viewController.clientOutput as! SocialEventClientPresenter
        XCTAssertNotNil(presenter.view, "view in SocialEventClientPresenter is nil after assembly")
        XCTAssertNotNil(presenter.router, "router in SocialEventClientPresenter is nil after assembly")
        XCTAssertTrue(presenter.router is SocialEventClientRouter, "router is not SocialEventClientRouter")

        let interactor: SocialEventClientInteractor = presenter.interactor as! SocialEventClientInteractor
        XCTAssertNotNil(interactor.output, "output in SocialEventClientInteractor is nil after assembly")
    }

    class SocialEventClientViewControllerMock: SocialEventViewController {

    }
}
