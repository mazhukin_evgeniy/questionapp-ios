//  Copyright © 2019 SWTecNN. All rights reserved.

import XCTest
@testable import QuestionsApp

class SocialEventClientPresenterTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    class MockInteractor: ISocialEventClientInteractor {
        func obtainRaiseHandRequestStatus(for socialEventId: String) {
            
        }
        
        func obtainName() {
            
        }
        
        func archiveSocialEvent(with id: String) {
            
        }
        
        func obtainSocialEvent(with id: String) {
            
        }
        
        func createQuestion(_ question: Question) {
            
        }
        
        func createRaiseHandRequest(for socialEventId: String) {
            
        }
        
        func cancelRaiseHandRequest(for socialEventId: String) {
            
        }
    }

    class MockRouter: ISocialEventClientRouter {
        func showSocialEventInfo(with id: String) {
            
        }
        
        func showQuestionsFor(socialId: String) {
            
        }
        

    }

    class MockViewController: ISocialEventClientView {
        func hideRaiseHandRequestStatus() {
            
        }
        
        func setDefaultName(_ name: String) {
            
        }
        
        func showProgress() {
            
        }
        
        func hideProgress() {
            
        }
        
        func showRaiseHandRequest(status: RaiseHandRequestStatus) {
            
        }
        
        func showSocialEventInfo(_ socialEvent: SocialEvent) {
        }
        
        func handleError(_ error: Error) {
        }
    }
}
