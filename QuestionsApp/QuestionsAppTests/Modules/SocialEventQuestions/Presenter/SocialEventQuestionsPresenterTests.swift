//  Copyright © 2019 SWTecNN. All rights reserved.

import XCTest
@testable import QuestionsApp

class SocialEventQuestionsPresenterTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    class MockInteractor: ISocialEventQuestionsInteractor {
        
        func obtainQuestions(for eventId: String) {
            
        }
        
        func updateVoteForQuestion(with questionId: String, vote: Int) {
            
        }
        
        func deleteQuestion(with questionId: String) {
            
        }
        
        func archiveQuestion(with questionId: String) {
            
        }
    }

    class MockRouter: ISocialEventQuestionsRouter {

    }

    class MockViewController: ISocialEventQuestionsView {
        
        func showProgress() {

        }
        
        func hideProgress() {
            
        }

        func showQuestions(_ questions: [SocialEventQuestion], for role: Role) {
            
        }
        
        func updateQuestions(_ questions: [SocialEventQuestion], andMoveAt index: Int, to toIndex: Int) {
            
        }
        
        func updateQuestions(_ questions: [SocialEventQuestion], andRemoveAt index: Int) {
            
        }
        
        func reloadQuestion(at index: Int) {
            
        }
        
        func showQuestion(at index: Int) {
            
        }
        
        func handleError(_ error: Error) {
            
        }
    }
}
