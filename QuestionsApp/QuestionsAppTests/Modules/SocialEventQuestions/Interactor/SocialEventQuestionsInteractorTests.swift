//  Copyright © 2019 SWTecNN. All rights reserved.

import XCTest
@testable import QuestionsApp

class SocialEventQuestionsInteractorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    class MockPresenter: ISocialEventQuestionsInteractorOutput {
        
        func questionsObtained(_ questions: [SocialEventQuestion]) {
            
        }
        
        func questionsObtainError(_ error: Error) {
            
        }

        func updateVoteError(_ error: Error, for questionId: String) {
            
        }
        
        func deleteQuestionError(_ error: Error, for questionId: String) {
            
        }
        
        func archiveQuestionError(_ error: Error, for questionId: String) {
            
        }

    }
}
