//  Copyright © 2018 SWTecNN. All rights reserved.

import XCTest
@testable import QuestionsApp

class ApplicationInteractorTests: XCTestCase {
    
    var interactor: IApplicationInteractor!
    
    var state: MockApplicationState!
    var presenter: MockPresenter!
    var analyticsRegistry: MockAnalyticsRegistry!
    
    override func setUp() {
        super.setUp()
        
        state = MockApplicationState()
        presenter = MockPresenter()
        analyticsRegistry = MockAnalyticsRegistry()
        
        let interactor = ApplicationInteractor(state: state, analyticsRegistry: analyticsRegistry)
        interactor.output = presenter
        
        self.interactor = interactor
    }
    
    func test_changeStateToStartingWhenStart() {
        
        assertNotThrows {
            
            try interactor.start()
            
            XCTAssertTrue(state.startingStateWasSet)
        }
    }

    
    func test_changeStateToRunningWhenStart() {
        
        assertNotThrows {
            
            try interactor.start()
            
            XCTAssert(state.resultState == .running)
        }
    }
    
    func test_resendStateChangesToOutput() {
        
        assertNotThrows {
            
            try interactor.start()
            
            state.listener?.stateChanged(to: .notStarted, from: .starting)
            
            XCTAssertTrue(presenter.applicationStateChangedWasCalled)
        }
    }
    
    func test_registerStartEvent() {
        
        assertNotThrows {
            
            try interactor.start()
            
            XCTAssertTrue(analyticsRegistry.startEvent_wasRegistered)
        }
    }

    class MockPresenter: IApplicationInteractorOutput {
        
        var applicationStateChangedWasCalled = false
        func applicationStateChanged(to: ApplicationState, from: ApplicationState) {
            
            applicationStateChangedWasCalled = true
        }
    }
    
    class MockApplicationState: IApplicationState {
        
        var listener: IApplicationStateListener?
        var current: ApplicationState
        
        init() {
            current = .notStarted
        }
        
        var startingStateWasSet = false
        var resultState: ApplicationState = .notStarted
        func changeState(to: ApplicationState) {
            
            resultState = to
            switch to {
            case .starting:
                startingStateWasSet = true
            default:
                break
            }
        }
        
        func addListener(_ listener: IApplicationStateListener) {
            self.listener = listener
        }
        
        func removeListener(_ listener: IApplicationStateListener) {
            self.listener = nil
        }
        
        
    }
    
    class MockAnalyticsRegistry: AnalyticsEventRegistry {
        
        var startEvent_wasRegistered = false
        func register(event: AnalyticsEvent) {
            
            if case .start = event {
                startEvent_wasRegistered = true
            }
        }
    }
}
