//  Copyright © 2018 SWTecNN. All rights reserved.

import XCTest
@testable import QuestionsApp

class ApplicationPresenterTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    class MockInteractor: IApplicationInteractor {
        func enterBackground() {
        }
        
        func enterForeground() {
        }
        
        func becomeActive() {
        }
        
        func terminate() {
        }
        
        func start() throws {
        }
        

    }

    class MockRouter: IApplicationRouter {
        func showMain() {
        
        }
    }

    class MockViewController: IApplicationView {
        func showProgress() {
        }
        
        func hideProgress() {
        }
        
        func showInitializationError() {
        }
        
        func setupInitialState() {

        }
    }
}
