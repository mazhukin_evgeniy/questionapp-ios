//  Copyright © 2019 SWTecNN. All rights reserved.

import XCTest
@testable import QuestionsApp

class SocialEventAdminModuleAssemblyTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testAssemblyModuleForViewController() {

        //given
        let viewController = SocialEventAdminViewControllerMock()
        let assembly = SocialEventAdminAssembly()

        //when
        assembly.assemblyModuleForView(view: viewController)

        //then
        XCTAssertNotNil(viewController.adminOutput, "SocialEventAdminViewController is nil after assembly")
        XCTAssertTrue(viewController.adminOutput is SocialEventAdminPresenter, "output is not SocialEventAdminPresenter")

        let presenter: SocialEventAdminPresenter = viewController.adminOutput as! SocialEventAdminPresenter
        XCTAssertNotNil(presenter.view, "view in SocialEventAdminPresenter is nil after assembly")
        XCTAssertNotNil(presenter.router, "router in SocialEventAdminPresenter is nil after assembly")
        XCTAssertTrue(presenter.router is SocialEventAdminRouter, "router is not SocialEventAdminRouter")

        let interactor: SocialEventAdminInteractor = presenter.interactor as! SocialEventAdminInteractor
        XCTAssertNotNil(interactor.output, "output in SocialEventAdminInteractor is nil after assembly")
    }

    class SocialEventAdminViewControllerMock: SocialEventViewController {

    }
}
