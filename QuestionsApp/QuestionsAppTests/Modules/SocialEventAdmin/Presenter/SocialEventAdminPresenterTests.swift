//  Copyright © 2019 SWTecNN. All rights reserved.

import XCTest
@testable import QuestionsApp

class SocialEventAdminPresenterTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    class MockInteractor: ISocialEventAdminInteractor {
        func notifyHandRequest(_ handRequest: RaiseHandRequest) {
            
        }
        
        func obtainNextHandRequestForSocialEvent(id: String) {
            
        }
        
        func finishHandRequest(id: String) {
            
        }
        
        func finishSocialEvent(with id: String) {
            
        }
        
        func obtainQuestionsCountForSocialEvent(id: String) {
            
        }
        
        func obtainHandRequestsCountForSocialEvent(id: String) {
            
        }
        
        func obtainSocialEvent(with id: String) {
            
        }
        

    }

    class MockRouter: ISocialEventAdminRouter {
        func showSocialEventInfo(with id: String) {
            
        }
        
        func showQuestionsFor(socialId: String) {
            
        }
        
        func close() {
            
        }
        

    }

    class MockViewController: ISocialEventAdminView {
        func showActiveHandRequest(_ handRequest: RaiseHandRequest) {
            
        }
        
        func showProgress() {
            
        }
        
        func hideProgress() {
            
        }
        
        func showQuestion(count: Int) {
            
        }
        
        func showHandRequests(count: Int) {
            
        }
        
        func showSocialEventInfo(_ socialEvent: SocialEvent) {
            
        }
        
        func handleError(_ error: Error) {
            
        }
        

        func setupInitialState() {

        }
    }
}
