// Copyright © 2019 SWTecNN. All rights reserved.

import UIKit

extension UIColor {
    
    static var sectionTitle = UIColor(named: "sectionTitle")!
    static var sectionBody = UIColor(named: "sectionBody")!
    
    static var lightButtonTitle = UIColor(named: "lightButtonTitle")!
    static var lightButtonBackground = UIColor(named: "lightButtonBackground")!
    
    static var defaultButtonTitle = UIColor(named: "defaultButtonTitle")!
    static var defaultButtonBackground = UIColor(named: "defaultButtonBackground")!
    
    static var cancelButtonTitle = UIColor(named: "cancelButtonTitle")!
    static var cancelButtonBackground = UIColor(named: "cancelButtonBackground")!
    
    static var blueButtonTitle = UIColor(named: "blueButtonTitle")!
    static var blueButtonBackground = UIColor(named: "blueButtonBackground")!
    
    static var inactiveColor = UIColor(named: "inactiveColor")!  
}
