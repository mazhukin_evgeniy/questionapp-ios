// Copyright © 2019 SWTecNN. All rights reserved.

import UIKit

extension UIFont {
    
    static let buttonTitle = UIFont.systemFont(ofSize: 17, weight: .semibold)
    static let sectionTitle = UIFont.systemFont(ofSize: 13, weight: .bold)
    static let alertTitle = UIFont.boldSystemFont(ofSize: 17)
    static let alertMessage = UIFont.systemFont(ofSize: 13)
}
