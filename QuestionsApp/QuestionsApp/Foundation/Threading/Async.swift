// Copyright © 2018 SWTecNN. All rights reserved.

import Foundation

public func async(f: @escaping () -> Void) {
    
    DispatchQueue.global(qos: .default).async(execute: f)
}

public func asyncMain(f: @escaping () -> Void) {
    
    DispatchQueue.main.async(execute: f)
}
