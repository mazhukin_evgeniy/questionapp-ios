// Copyright © 2018 SWTecNN. All rights reserved.

import Foundation

public protocol Semaphore {
    
    func wait(timeout: TimeInterval?) -> Bool
}

public extension Semaphore {
    
    func wait() {
        _ = self.wait(timeout: nil)
    }
}

public struct BinarySemaphore: Semaphore {
    
    private let group: DispatchGroup = DispatchGroup()
    
    public init() {
        group.enter()
    }
    
    public func wait(timeout: TimeInterval?) -> Bool {
        
        let dispatchTimeout = timeout.map { DispatchTime.adding($0) } ?? DispatchTime.distantFuture
        return group.wait(timeout: dispatchTimeout) == DispatchTimeoutResult.success
    }
    
    public func signal() {
        group.leave()
    }
}

public struct CountingSemaphore: Semaphore {
    
    private let semaphore: DispatchSemaphore
    
    public init(state: Int = 0) {
        semaphore = DispatchSemaphore(value: state)
    }
    
    public func wait(timeout: TimeInterval?) -> Bool {
        
        let dispatchTimeout = timeout.map { DispatchTime.adding($0) } ?? DispatchTime.distantFuture
        return semaphore.wait(timeout: dispatchTimeout) == DispatchTimeoutResult.success
    }
    
    public func signal() {
        semaphore.signal()
    }
}
