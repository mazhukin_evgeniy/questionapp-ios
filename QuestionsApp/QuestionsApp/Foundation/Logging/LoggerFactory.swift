// Copyright © 2018 SWTecNN. All rights reserved.

import Foundation

public protocol LoggerFactory {
    
    func getLogger(for domain: LogDomain) -> Logger
}

class DefaultLoggerFactory: LoggerFactory {
    
    private let disabledLogger: Logger = {
        let logger = Logger()
        logger.level = .off
        return logger
    }()
    
    private let disabledDomains: [LogDomain]
    private let logger: Logger
    
    init() {
        disabledDomains = []
        logger = disabledLogger
    }
    
    init(configuration: LogConfiguration) {
        
        disabledDomains = configuration.disabledDomains
        
        let logger = Logger()
        logger.level = configuration.level
        
        #if DEBUG
        logger.with(writer: ConsoleLogWriter())
        #endif
        
        logger.with(writer: FileLogWriter(fileNamePrefix: configuration.fileNamePrefix,
                                          logsDirectory: configuration.logsDirectory,
                                          rollingFrequency: configuration.rollingFrequency,
                                          maxFilesCount: configuration.maxFilesCount))
        
        self.logger = logger
    }
    
    func getLogger(for domain: LogDomain) -> Logger {
        
        if disabledDomains.contains(domain) {
            
            return disabledLogger
        }
        
        return logger
    }
}
