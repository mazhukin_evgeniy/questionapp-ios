// Copyright © 2018 SWTecNN. All rights reserved.

import Foundation

public struct LogDomain: RawRepresentable {
    
    public let rawValue: String
    
    public init(rawValue: String) {
        self.rawValue = rawValue
    }
}

extension LogDomain: ExpressibleByStringLiteral {
    public init(stringLiteral value: String) {
        self.init(rawValue: value)
    }
    
    public init(extendedGraphemeClusterLiteral value: String) {
        self.init(rawValue: value)
    }
    
    public init(unicodeScalarLiteral value: String) {
        self.init(rawValue: value)
    }
}

extension LogDomain {
    
    public static var `default`: LogDomain = "com.swtecnn"
}

extension LogDomain: Comparable {
    public static func < (lhs: LogDomain, rhs: LogDomain) -> Bool {
        return lhs.rawValue < rhs.rawValue
    }
}
