// Copyright © 2018 SWTecNN. All rights reserved.

import Foundation

open class DefaultLogFormatter: LogFormatter {
    
    private let dateFormatter: DateFormatterProtocol
    
    public init(dateFormatter: DateFormatterProtocol = DateFormatter(format: "HH:mm:ss.SSS")) {
        
        self.dateFormatter = dateFormatter
    }
    
    open func format(message: String, level: Logger.Level, error: Error?, timestamp: Date, file: String, function: String, line: UInt) -> String {
        
        return "\(dateFormatter.string(from: timestamp)) \(representation(of: level)) [\(PathUtils.name(path: file)):\(line)] - \(representation(of: message, error: error))"
    }
    
    private func representation(of level: Logger.Level) -> String {
        
        switch level {
        case .off: return ""
        case .error: return "🛑 ERRO"
        case .warning: return "⚠️ WARN"
        case .info: return "💬 INFO"
        case .debug: return "↪️ DEBG"
        case .verbose: return "🔎 VERB"
        }
    }
    
    private func representation(of message: String, error: Error?) -> String {
        
        if let error = error {
            return "\(message)\n\(error)"
        } else {
            return message
        }
    }
}

