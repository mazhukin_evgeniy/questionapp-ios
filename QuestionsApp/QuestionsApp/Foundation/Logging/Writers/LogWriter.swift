// Copyright © 2018 SWTecNN. All rights reserved.

import Foundation

public protocol LogWriter {
    
    func write(message: String)
    func flush()
}
