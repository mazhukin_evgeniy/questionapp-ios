// Copyright © 2018 SWTecNN. All rights reserved.

import Foundation

struct LogConfiguration {
    
    let level: Logger.Level
    let disabledDomains: [LogDomain]
    let fileNamePrefix: String
    let logsDirectory: URL
    let rollingFrequency: TimeInterval
    let maxFilesCount: Int
}
