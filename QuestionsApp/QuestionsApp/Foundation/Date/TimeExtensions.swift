// Copyright © 2018 SWTecNN. All rights reserved.

import Foundation

extension DispatchTime {
    
    public static func adding(_ timeInterval: TimeInterval) -> DispatchTime {
        
        let timeIntervalInNanoseconds = UInt64(timeInterval) * 1000000000
        
        return DispatchTime(uptimeNanoseconds: DispatchTime.now().uptimeNanoseconds + timeIntervalInNanoseconds)
    }
}

extension TimeInterval {
    
    public init(minutes: Int) {
        self.init(minutes * 60)
    }
}
