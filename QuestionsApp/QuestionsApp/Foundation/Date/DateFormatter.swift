// Copyright © 2018 SWTecNN. All rights reserved.

import Foundation

public protocol DateFormatterProtocol {
    
    func date(from string: String) -> Date?
    
    func string(from date: Date) -> String
}

open class DateFormatter: DateFormatterProtocol {
    
    private let formatter: Foundation.DateFormatter
    
    public init(format: String, timeZone: TimeZone? = nil) {
        
        formatter = Foundation.DateFormatter()
        formatter.dateFormat = format
        
        if let timeZone = timeZone {
            formatter.timeZone = timeZone
        }
    }
    
    open func string(from date: Date) -> String {
        
        return formatter.string(from: date)
    }
    
    open func date(from string: String) -> Date? {
        
        return formatter.date(from: string)
    }
}

extension DateFormatter {
    public static let `default` = DateFormatter(format: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    public static let gmt = DateFormatter(format: "EEE, dd MMM yyyy HH:mm:ss 'GMT'", timeZone: TimeZone(secondsFromGMT: 0))
}
