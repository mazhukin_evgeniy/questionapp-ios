// Copyright © 2018 SWTecNN. All rights reserved.

import Foundation

public enum DateProvider {
    
    public static func now() -> Date {
        return Date()
    }
}
