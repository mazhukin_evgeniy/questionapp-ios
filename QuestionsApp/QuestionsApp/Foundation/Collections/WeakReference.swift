// Copyright © 2018 SWTecNN. All rights reserved.

import Foundation

public struct WeakReference<T: AnyObject> {
    
    private(set) weak var value: T?
    
    public init(_ value: T) {
        self.value = value
    }
}
