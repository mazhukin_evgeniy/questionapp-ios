// Copyright © 2018 SWTecNN. All rights reserved.

import Foundation

public struct WeakArray<Element> where Element: AnyObject {
    
    private var storage: [WeakReference<Element>] = []
    
    public mutating func append(_ newElement: Element) {
        storage.append(WeakReference(newElement))
        
        reap()
    }
    
    public mutating func remove(_ element: Element) {
        
        if let index = storage.firstIndex(where: {$0.value === element }) {
            
            storage.remove(at: index)
        }
        
        reap()
    }

    public mutating func reap() {
        
        storage = storage.filter { $0.value != nil }
    }
    
    public subscript(position: Int) -> Element? {
        return storage[position].value
    }
}

extension WeakArray: Sequence {
    
    public func makeIterator() -> WeakArrayIterator<Element> {
        
        return WeakArrayIterator(storage: storage)
    }
}

public struct WeakArrayIterator<T: AnyObject>: IteratorProtocol {
    
    public typealias Element = T
    
    private let storage: [WeakReference<T>]
    private var index: Int
    
    public init(storage: [WeakReference<T>]) {
        
        self.storage = storage
        index = storage.startIndex
    }
    
    public mutating func next() -> T? {
        
        if index < storage.endIndex {
            
            let valueToCheck = storage[index].value
            
            index = storage.index(after: index)
            
            if let value = valueToCheck {
                return value
            } else {
                return next()
            }
            
        } else {
            return nil
        }
    }
}
