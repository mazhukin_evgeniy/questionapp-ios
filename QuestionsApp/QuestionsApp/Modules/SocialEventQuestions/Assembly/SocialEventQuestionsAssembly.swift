//  Copyright © 2019 SWTecNN. All rights reserved.

import UIKit

class SocialEventQuestionsAssembly {

    func assemblyModuleForView<UIViewController>(view: UIViewController) {

        if let viewController = view as? SocialEventQuestionsViewController {
            assembly(viewController: viewController)
        }
    }

    private func assembly(viewController: SocialEventQuestionsViewController) {

        let router = SocialEventQuestionsRouter()

        let presenter = SocialEventQuestionsPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let questionRepository = Service.factory.getQuestionRepository()
        let clientQuestionRepository = Service.factory.getClientQuestionRepository()

        let interactor = SocialEventQuestionsInteractor(questionRepository: questionRepository,
                                                        clientQuestionRepository: clientQuestionRepository)
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
        viewController.moduleInput = presenter
    }

}
