// Copyright © 2019 SWTecNN. All rights reserved.

import Foundation

public class SocialEventQuestion {
    let id: String
    let socialEventId: String
    let author: String?
    let description: String
    let own: Bool
    
    var archived: Bool
    
    var voteCount: Int
    var voted: Bool

    init(id: String,
         socialEventId: String,
         author: String?,
         description: String,
         voteCount: Int,
         voted: Bool,
         own: Bool,
         archived: Bool) {
        self.id = id
        self.socialEventId = socialEventId
        self.author = author
        self.description = description
        self.voteCount = voteCount
        self.voted = voted
        self.own = own
        self.archived = archived
    }
}

extension SocialEventQuestion {
    var displayName: String {
        return author ?? lcMsg("default_name")
    }
}
