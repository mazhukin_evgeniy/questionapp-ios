//  Copyright © 2019 SWTecNN. All rights reserved.

protocol ISocialEventQuestionsViewOutput {

    func viewIsReady()
    
    func updateVotePressed(for question: SocialEventQuestion)
    func revokePressed(for questionId: String)
    func deletePressed(for questionId: String)
    func archivePressed(for questionId: String)
    func questionSelected(at index: Int)
}
