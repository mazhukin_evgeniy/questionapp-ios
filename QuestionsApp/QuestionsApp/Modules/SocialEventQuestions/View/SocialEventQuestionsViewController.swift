//  Copyright © 2019 SWTecNN. All rights reserved.

import UIKit

class SocialEventQuestionsViewController: BaseViewController {
    
    private let questionCellId = "QuestionCell"

    var output: ISocialEventQuestionsViewOutput!

    @IBOutlet weak var tableView: UITableView!
    
    var questions = [SocialEventQuestion]()
    var role: Role = .viewer
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = lcMsg("social_event_questions")
        setupTableView()
        output.viewIsReady()
    }
    
    private func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    private func updateTableView(with questions: [SocialEventQuestion]) {
        self.questions = questions
        tableView.reloadData()
    }
}

extension SocialEventQuestionsViewController: ISocialEventQuestionsView {
    
    func showQuestions(_ questions: [SocialEventQuestion], for role: Role) {
        self.role = role
        updateTableView(with: questions)
    }
    
    func updateQuestions(_ questions: [SocialEventQuestion], andMoveAt index: Int, to toIndex: Int) {
        let indexPath = IndexPath(row: index, section: 0)
        let toIndexPath = IndexPath(row: toIndex, section: 0)
        
        tableView.reloadRows(at: [indexPath], with: .automatic)
        tableView.moveRow(at: indexPath, to: toIndexPath)
        
        self.questions = questions
    }
    
    func updateQuestions(_ questions: [SocialEventQuestion], andRemoveAt index: Int) {
        self.questions = questions
        
        let indexPath = IndexPath(row: index, section: 0)
        tableView.deleteRows(at: [indexPath], with: .fade)
    }
    
    func reloadQuestion(at index: Int) {
        let indexPath = IndexPath(row: index, section: 0)
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    func showQuestion(at index: Int) {
        let question = questions[index]
        
        guard !question.archived else {
            return
        }

        let votesText = "\(lcMsg("question_votes")): \(question.voteCount)"
        let message = "\n\(votesText)\n\n\(question.description)"
        
        let alert = UIAlertController(title: question.displayName,
                                      message: message,
                                      preferredStyle: .alert)
        
        alert.setTitleFont(.alertTitle, color: .black)
        alert.setMessageFont(.alertMessage, color: .sectionTitle)
        
        let closeAction = UIAlertAction(title: lcMsg("question_close"), style: .cancel, handler: nil)
        alert.addAction(closeAction)

        if role == .admin {
            let deleteAction = UIAlertAction(title: lcMsg("question_delete"), style: .destructive) { action in
                self.output.deletePressed(for: question.id)
            }
            alert.addAction(deleteAction)
            
            let archiveAction = UIAlertAction(title: lcMsg("question_answered"), style: .default) { action in
                self.output.archivePressed(for: question.id)
            }
            alert.addAction(archiveAction)
        } else if role == .client {
            if question.own {
                let revokeAction = UIAlertAction(title: lcMsg("question_revoke"), style: .destructive) { action in
                    self.output.revokePressed(for: question.id)
                }
                alert.addAction(revokeAction)
            } else {
                let title = question.voted ? lcMsg("question_voted") : lcMsg("question_vote")
                let voteAction = UIAlertAction(title: title, style: .default) { action in
                    self.output.updateVotePressed(for: question)
                }
                voteAction.isEnabled = !question.voted
                alert.addAction(voteAction)
            }
        }

        present(alert, animated: true)
    }
}

extension SocialEventQuestionsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: questionCellId, for: indexPath) as? QuestionTableViewCell else {
            return UITableViewCell()
        }
        
        let question = questions[indexPath.row]
        cell.setupWithQuestion(question)
        
        guard !question.archived else {
            return cell
        }
        
        if role == .admin {
            cell.setPrimaryButtonTitle(lcMsg("question_delete"), mode: .cancel) { [weak self] in
                self?.output.deletePressed(for: question.id)
            }
            cell.setSecondaryButtonTitle(lcMsg("question_answered"), mode: .blue) { [weak self] in
                self?.output.archivePressed(for: question.id)
            }
        } else if role == .client {
            if question.own {
                cell.setPrimaryButtonTitle(lcMsg("question_revoke"), mode: .cancel) { [weak self] in
                    self?.output.revokePressed(for: question.id)
                }
            } else {
                let mode: Button.Mode = question.voted ? .light : .blue
                cell.setPrimaryButtonTitle(lcMsg("question_vote"), mode: mode) { [weak self] in
                    self?.output.updateVotePressed(for: question)
                }
            }
        }

        return cell
    }
}

extension SocialEventQuestionsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        output.questionSelected(at: indexPath.row)
    }
}
