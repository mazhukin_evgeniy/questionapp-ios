//  Copyright © 2019 SWTecNN. All rights reserved.

protocol ISocialEventQuestionsView: class, IProgressableView, IErrorView {

    func showQuestions(_ questions: [SocialEventQuestion], for role: Role)

    func updateQuestions(_ questions: [SocialEventQuestion], andMoveAt index: Int, to toIndex: Int)
    func updateQuestions(_ questions: [SocialEventQuestion], andRemoveAt index: Int)
    func reloadQuestion(at index: Int)
    func showQuestion(at index: Int)
}

