//  Copyright © 2019 SWTecNN. All rights reserved.

class SocialEventInfoRouter: ISocialEventInfoRouter {

    var transitionHandler: IModuleTransitionHandler!

    func openSocialEventQuestions(with id: String, role: Role) {

        transitionHandler.openModule(id: "showSocialEventQuestions") { (module) in
            guard let input = module as? ISocialEventQuestionsModule else { return }

            input.socialEventId = id
            input.role = role
        }
    }

    func openSocialEventMaterial(_ material: String) {

        ApplicationUtils.openUrl(material)
    }
}
