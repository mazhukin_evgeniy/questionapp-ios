//  Copyright © 2019 SWTecNN. All rights reserved.

import Foundation

protocol ISocialEventInfoRouter {

    func openSocialEventQuestions(with id: String, role: Role)
    func openSocialEventMaterial(_ material: String)
}
