//  Copyright © 2019 SWTecNN. All rights reserved.

import Foundation

protocol ISocialEventInfoInteractorOutput: class {

    func socialEventObtained(_ socialEvent: SocialEvent)
    func socialEventObtainError(_ error: Error)
}
