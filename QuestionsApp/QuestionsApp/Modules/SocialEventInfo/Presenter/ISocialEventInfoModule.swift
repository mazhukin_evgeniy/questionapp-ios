//  Copyright © 2019 SWTecNN. All rights reserved.

protocol ISocialEventInfoModule: class {

    var socialEventId: String! { get set }
    var role: Role! { get set }
}
