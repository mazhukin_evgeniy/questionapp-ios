//  Copyright © 2019 SWTecNN. All rights reserved.

protocol ISocialEventInfoView: class, IProgressableView, IErrorView {

    func showSocialEventInfo(_ socialEvent: SocialEvent)
    func enableQuestions()
}
