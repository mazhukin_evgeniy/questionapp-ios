//  Copyright © 2019 SWTecNN. All rights reserved.

import UIKit

enum SocialEventInfoItemType: String {

    case topic = "social_event_topic"
    case date = "social_event_date"
    case lector = "social_event_lector"
    case description = "social_event_description"
    case agenda = "social_event_agenda"
    case usefulLinks = "social_event_useful_links"
}

struct SocialEventInfoItem {

    let type: SocialEventInfoItemType
    let value: Any
}

class SocialEventInfoViewController: BaseViewController {

    private let socialEventInfoItemCellId = "SocialEventInfoItemCell"
    private let socialEventInfoMatherialCellId = "SocialEventInfoMatherialCell"

    @IBOutlet weak var tableView: UITableView!

    var output: ISocialEventInfoViewOutput!

    var socialEventInfoItems = [SocialEventInfoItem]()

    override func viewDidLoad() {

        super.viewDidLoad()
        
        setupNavigationItem()

        setupTableView()

        output.viewIsReady()
    }

    private func setupTableView() {

        tableView.dataSource = self
        tableView.delegate = self
    }
    
    private func setupNavigationItem() {
        navigationItem.backBarButtonItem =
            UIBarButtonItem(title: lcMsg("social_event_lecture"),
                            style: .plain,
                            target: nil,
                            action: nil)
    }

    private func updateTableView(with socialEvent: SocialEvent) {

        let dateFormatter = DateFormatter(format: "dd.MM.YYYY HH:mm")
        socialEventInfoItems = [
            SocialEventInfoItem(type: .topic, value: socialEvent.title),
            SocialEventInfoItem(type: .date, value: dateFormatter.string(from: socialEvent.date)),
            SocialEventInfoItem(type: .lector, value: socialEvent.lector),
            SocialEventInfoItem(type: .description, value: socialEvent.description),
            SocialEventInfoItem(type: .agenda, value: socialEvent.agenda),
            SocialEventInfoItem(type: .usefulLinks, value: socialEvent.usefulLinks)
        ]
        tableView.reloadData()
    }

    @objc private func questionsPressed() {

        output.socialEventQuestionsPressed()
    }
}

extension SocialEventInfoViewController: ISocialEventInfoView {

    func showSocialEventInfo(_ socialEvent: SocialEvent) {

        title = socialEvent.title
        updateTableView(with: socialEvent)
    }

    func enableQuestions() {

        let questionsButton = UIBarButtonItem(title: lcMsg("social_event_questions"), style: .done, target: self, action: #selector(questionsPressed))
        navigationItem.rightBarButtonItem = questionsButton
    }
}

extension SocialEventInfoViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {

        return socialEventInfoItems.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        let socialEventInfoItem = socialEventInfoItems[section]
        if let valueArray = socialEventInfoItem.value as? [String] {
            return valueArray.count
        } else if socialEventInfoItem.value is String {
            return 1
        } else {
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let socialEventInfoItem = socialEventInfoItems[indexPath.section]

        var cellId = socialEventInfoItemCellId
        var title = socialEventInfoItem.value as? String
        if socialEventInfoItem.type == .usefulLinks {
            cellId = socialEventInfoMatherialCellId
            title = (socialEventInfoItem.value as? [String])?[indexPath.row]
        }

        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? TextTableViewCell else {
            return UITableViewCell()
        }
        cell.setTitle(title)
        return cell
    }
}

extension SocialEventInfoViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let label = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 20))
        label.font = .sectionTitle
        label.textColor = .sectionTitle
        label.text = lcMsg(socialEventInfoItems[section].type.rawValue)
        return label
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {

        return 20
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let socialEventInfoItem = socialEventInfoItems[indexPath.section]
        guard socialEventInfoItem.type == .usefulLinks, let material = (socialEventInfoItem.value as? [String])?[indexPath.row] else {
            return
        }
        output.socialEventMaterialPressed(material)
    }
}
