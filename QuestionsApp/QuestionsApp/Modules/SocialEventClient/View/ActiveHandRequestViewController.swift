// Copyright © 2019 SWTecNN. All rights reserved.

import UIKit

class ActiveHandRequestViewController: UIViewController {
    
    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet private var dateLabel: UILabel!
    
    public var handRequest: RaiseHandRequest?
    public var onFinish: ((String)-> Void)?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let handRequest = handRequest else { return }
        
        nameLabel.text = handRequest.name
        dateLabel.text = DateFormatter.default.string(from: handRequest.timestamp)
    }
    
    @IBAction func finishPressed() {
        guard let requestId = handRequest?.id else { return }
        onFinish?(requestId)
    }
}
