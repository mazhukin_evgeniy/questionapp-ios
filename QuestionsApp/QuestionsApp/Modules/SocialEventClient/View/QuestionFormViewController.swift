// Copyright © 2019 SWTecNN. All rights reserved.

import UIKit

class QuestionFormViewController: UIViewController {
    
    struct Question {
        let name: String?
        let slide: Int?
        let question: String
    }
    
    var onResult: ((Question) -> Void)?
    var defaultName: String? {
        didSet {
            updateName()
        }
    }

    @IBOutlet private var nameField: UITextField?
    @IBOutlet private var slideField: UITextField!
    @IBOutlet private var questionField: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "create_question_title".lcMsg()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateName()
    }
    
    private func updateName() {
        nameField?.text = defaultName
    }
    
    @IBAction private func createQuestionPressed() {
        
        if questionField.text.count == 0 {
            
            showAlert(with: "create_question_title".lcMsg(), and: "create_question_empty_form_error".lcMsg())
            return
        }
        
        onResult?(Question(name: nameField?.text,
                           slide: Int(slideField.text ?? ""),
                           question: questionField.text))
    }
}
