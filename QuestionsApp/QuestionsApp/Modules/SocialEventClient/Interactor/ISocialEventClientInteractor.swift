//  Copyright © 2019 SWTecNN. All rights reserved.

import Foundation

protocol ISocialEventClientInteractor {

    func obtainSocialEvent(with id: String)
    func archiveSocialEvent(with id: String)
    func obtainName()
    
    func createQuestion(_ question: Question)
    
    func createRaiseHandRequest(for socialEventId: String)
    func cancelRaiseHandRequest(for socialEventId: String)
    
    func obtainRaiseHandRequestStatus(for socialEventId: String)
}
