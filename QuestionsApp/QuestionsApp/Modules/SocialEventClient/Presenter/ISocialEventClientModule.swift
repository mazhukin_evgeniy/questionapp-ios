//  Copyright © 2019 SWTecNN. All rights reserved.

protocol ISocialEventClientModule: class {

    var socialEventId: String! { get set }
}
