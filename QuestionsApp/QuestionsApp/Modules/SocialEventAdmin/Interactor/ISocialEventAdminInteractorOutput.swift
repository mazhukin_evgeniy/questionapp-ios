//  Copyright © 2019 SWTecNN. All rights reserved.

import Foundation

protocol ISocialEventAdminInteractorOutput: class {

    func socialEventObtained(_ socialEvent: SocialEvent)
    func socialEventObtainError(_ error: Error)
    
    func socialEventFinished()
    
    func handRequestObtained(_ handRequest: RaiseHandRequest)
    func handRequestError(_ error: Error)
    
    func questionCountObtained(_ count: Int)
    func handRequestCountObtained(_ count: Int)
}
