//  Copyright © 2019 SWTecNN. All rights reserved.

class SocialEventAdminPresenter: ISocialEventAdminModule {

    weak var view: ISocialEventAdminView!
    var interactor: ISocialEventAdminInteractor!
    var router: ISocialEventAdminRouter!
    
    var socialEventId: String!
}

extension SocialEventAdminPresenter: ISocialEventAdminInteractorOutput {
    
    func socialEventObtained(_ socialEvent: SocialEvent) {
        
        view.showSocialEventInfo(socialEvent)
        view.hideProgress()
    }
    
    func socialEventObtainError(_ error: Error) {
        view.hideProgress()
        view.handleError(error)
    }
    
    func socialEventFinished() {
        router.close()
    }
    
    func questionCountObtained(_ count: Int) {
        view.showQuestion(count: count)
    }
    
    func handRequestCountObtained(_ count: Int) {
        view.showHandRequests(count: count)
    }
    
    func handRequestObtained(_ handRequest: RaiseHandRequest) {
        view.showActiveHandRequest(handRequest)
        interactor.notifyHandRequest(handRequest)
        view.hideProgress()
    }
    
    func handRequestError(_ error: Error) {
        view.hideProgress()
        view.handleError(error)
    }
}

extension SocialEventAdminPresenter: ISocialEventAdminViewOutput {
    
    func socialEventInfoPressed() {
        router.showSocialEventInfo(with: socialEventId)
    }
    
    func finishSocialEventPressed() {
        interactor.finishSocialEvent(with: socialEventId)
    }
    
    func openQuestionsPressed() {
        router.showQuestionsFor(socialId: socialEventId)
    }
    
    func getHandRequestPressed() {
        view.showProgress()
        interactor.obtainNextHandRequestForSocialEvent(id: socialEventId)
    }
    
    func finishHandRequestPressed(with requestId: String) {
        interactor.finishHandRequest(id: requestId)
    }
    
    func viewIsShown() {
        
        view.showProgress()
        interactor.obtainSocialEvent(with: socialEventId)
        interactor.obtainQuestionsCountForSocialEvent(id: socialEventId)
        interactor.obtainHandRequestsCountForSocialEvent(id: socialEventId)
    }
}
