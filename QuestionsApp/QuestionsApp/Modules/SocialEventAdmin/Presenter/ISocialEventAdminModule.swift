//  Copyright © 2019 SWTecNN. All rights reserved.

protocol ISocialEventAdminModule: class {

    var socialEventId: String! { get set }
}
