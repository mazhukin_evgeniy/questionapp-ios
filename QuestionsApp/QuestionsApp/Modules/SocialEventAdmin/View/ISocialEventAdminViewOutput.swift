//  Copyright © 2019 SWTecNN. All rights reserved.

protocol ISocialEventAdminViewOutput {

    func viewIsShown()
    func socialEventInfoPressed()
    func finishSocialEventPressed()
    func openQuestionsPressed()
    func getHandRequestPressed()
    func finishHandRequestPressed(with requestId: String)
}
