//  Copyright © 2019 SWTecNN. All rights reserved.

import UIKit

class SocialEventViewController: BaseViewController {
    
    enum Mode {
        case admin
        case client
    }
    
    struct SegueIdentifier {
        static let createQuestion = "createQuestion"
        static let raiseHandRequestStatus = "raiseHandRequest"
        static let activeHandRequest = "activeHandRequest"
    }
    
    var adminOutput: ISocialEventAdminViewOutput?
    var clientOutput: ISocialEventClientViewOutput?
    
    var mode: Mode = .admin
    private var defaultName: String?

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var lectorLabel: UILabel!
    
    @IBOutlet var handRequestButton: Button!
    @IBOutlet var createQuestionButton: Button!
    @IBOutlet var questionsButton: Button!
    @IBOutlet var finishSocialEventButton: Button!
    
    public func intialize(mode: Mode) {
        self.mode = mode
        
        switch mode {
        case .admin:
            SocialEventAdminAssembly().assemblyModuleForView(view: self)
        case .client:
            SocialEventClientAssembly().assemblyModuleForView(view: self)
        }
    }
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        handRequestButton.modeValue = .default
        createQuestionButton.modeValue = .default
        
        questionsButton.setTitle(lcMsg("show_all_questions"), for: .normal)
        handRequestButton.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        adminOutput?.viewIsShown()
        clientOutput?.viewIsShown()
    }
    
    func showSocialEventInfo(_ socialEvent: SocialEvent) {
        
        titleLabel.text = socialEvent.title
        lectorLabel.text = socialEvent.lector
        
        handRequestButton.modeValue = .default
        if mode == .admin {
            questionsButton.modeValue = .default
            createQuestionButton.isHidden = true
            questionsButton.isHidden = false
            finishSocialEventButton.isHidden = false
        } else if mode == .client {
            questionsButton.isHidden = false
            questionsButton.modeValue = .light
            finishSocialEventButton.isHidden = true
            handRequestButton.isHidden = false
        }
    }
    
    @IBAction func questionsPressed() {
        adminOutput?.openQuestionsPressed()
        clientOutput?.openQuestionsPressed()
    }
    
    @IBAction func handRequestPressed() {
        adminOutput?.getHandRequestPressed()
        clientOutput?.raiseHandRequestPressed()
    }
    
    @IBAction func socialEventInfoPressed() {
        adminOutput?.socialEventInfoPressed()
        clientOutput?.socialEventInfoPressed()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch segue.identifier {
        case SegueIdentifier.createQuestion:
            guard let questionForm = segue.destination as? QuestionFormViewController else {
                break
            }
            
            questionForm.defaultName = defaultName
            questionForm.onResult = { [unowned self] (result) in
                self.clientOutput?.createQuestionPressed(name: result.name,
                                                         slide: result.slide,
                                                         question: result.question)
                self.navigationController?.popViewController(animated: true)
            }
        case SegueIdentifier.raiseHandRequestStatus:
            guard let statusView = segue.destination as? RaiseHandRequestStatusViewController,
                let status = sender as? RaiseHandRequestStatus else {
                break
            }
            
            statusView.onCancel = { [unowned self] in
                self.clientOutput?.cancelHandRequestPressed()
            }
            
            statusView.onRefresh = { [unowned self] in
                self.clientOutput?.updateHandRequestPressed()
            }
            
            statusView.status = status
            
        case SegueIdentifier.activeHandRequest:
            guard let activeHandRequestView = segue.destination as? ActiveHandRequestViewController,
                let request = sender as? RaiseHandRequest else {
                break
            }
            
            activeHandRequestView.onFinish = { [unowned self] (requestId) in
                
                self.adminOutput?.finishHandRequestPressed(with: requestId)
                self.navigationController?.popViewController(animated: true)
            }
            activeHandRequestView.handRequest = request
            
        default:
            break
        }
    }
    
    override func handleError(_ error: Error) {
        if case RaiseHandRepositoryError.empty = error {
            self.showErrorMessage(lcMsg("raise_hand_request_status_noRequests"))
        } else {
            super.handleError(error)
        }
    }
}

extension SocialEventViewController: ISocialEventAdminView {
    
    func showQuestion(count: Int) {
        let title = String(format: "show_all_questions_value".lcMsg(), count)
        questionsButton.setTitle(title, for: .normal)
    }
    
    func showHandRequests(count: Int) {
        handRequestButton.isHidden = false
        let title = String(format: "get_hand_request".lcMsg(), count)
        handRequestButton.setTitle(title, for: .normal)
    }
    
    func showActiveHandRequest(_ handRequest: RaiseHandRequest) {
        performSegue(withIdentifier: SegueIdentifier.activeHandRequest, sender: handRequest)
    }
    
    @IBAction func finishSocialInfoPressed() {
        adminOutput?.finishSocialEventPressed()
    }
    
}

extension SocialEventViewController: ISocialEventClientView {
    
    func showRaiseHandRequest(status: RaiseHandRequestStatus) {
        
        if let statusView = navigationController?.topViewController as? RaiseHandRequestStatusViewController {
            statusView.status = status
        } else {
            performSegue(withIdentifier: SegueIdentifier.raiseHandRequestStatus, sender: status)
        }
    }
    
    func hideRaiseHandRequestStatus() {
        if (navigationController?.topViewController as? RaiseHandRequestStatusViewController) != nil {
            navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func createQuestionPressed() {
        performSegue(withIdentifier: SegueIdentifier.createQuestion, sender: nil)
    }
    
    func setDefaultName(_ name: String) {
        defaultName = name
    }
}
