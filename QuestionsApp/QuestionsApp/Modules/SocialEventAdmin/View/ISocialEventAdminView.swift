//  Copyright © 2019 SWTecNN. All rights reserved.

protocol ISocialEventAdminView: class, IProgressableView, IErrorView {
    
    func showSocialEventInfo(_ socialEvent: SocialEvent)
    func showActiveHandRequest(_ handRequest: RaiseHandRequest)
    
    func showQuestion(count: Int)
    func showHandRequests(count: Int)
}
