//  Copyright © 2019 SWTecNN. All rights reserved.

import Foundation

protocol ISocialEventAdminRouter {

    func showSocialEventInfo(with id: String)
    func showQuestionsFor(socialId: String)
    func close()
}
