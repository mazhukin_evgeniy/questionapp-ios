//  Copyright © 2018 SWTecNN. All rights reserved.

import UIKit

class ApplicationModuleInitializer: NSObject {

    @IBOutlet weak var applicationViewController: ApplicationViewController!

    override func awakeFromNib() {

        let assembly = ApplicationAssembly()
        assembly.assemblyModuleForView(view: applicationViewController)
    }

}
