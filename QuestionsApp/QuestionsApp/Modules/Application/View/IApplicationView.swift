//  Copyright © 2018 SWTecNN. All rights reserved.

protocol IApplicationView: class {

    func setupInitialState()
    func showInitializationError()
    func showProgress()
    func hideProgress()
}
