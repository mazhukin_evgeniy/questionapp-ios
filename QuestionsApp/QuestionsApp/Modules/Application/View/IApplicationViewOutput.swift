//  Copyright © 2018 SWTecNN. All rights reserved.

protocol IApplicationViewOutput {

    func viewIsReady()
    
    func applicationDidEnterBackground()
    func applicationWillEnterForeground()
    func applicationDidBecomeActive()
    func applicationWillTerminate()
}
