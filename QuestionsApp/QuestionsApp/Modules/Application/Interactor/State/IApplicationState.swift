// Copyright © 2018 SWTecNN. All rights reserved.

import Foundation

protocol IApplicationState {
    
    var current: ApplicationState { get }
    
    func changeState(to: ApplicationState)
    
    func addListener(_ listener: IApplicationStateListener)
    func removeListener(_ listener: IApplicationStateListener)
}
