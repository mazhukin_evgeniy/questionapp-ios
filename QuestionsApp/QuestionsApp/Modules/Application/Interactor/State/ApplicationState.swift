// Copyright © 2018 SWTecNN. All rights reserved.

import Foundation

enum ApplicationState {
    
    case notStarted
    case starting
    case running
    case background
    case terminating
}
