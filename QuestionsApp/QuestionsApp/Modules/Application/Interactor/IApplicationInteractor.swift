//  Copyright © 2018 SWTecNN. All rights reserved.

import Foundation

protocol IApplicationInteractor {
    
    func start() throws
    func enterBackground()
    func enterForeground()
    func becomeActive()
    func terminate()
}
