//  Copyright © 2018 SWTecNN. All rights reserved.

class ApplicationInteractor: IApplicationInteractor {
    
    private let logger = Logger.factory.getLogger(for: .default)
    
    private let state: IApplicationState
    private let analyticsRegistry: AnalyticsEventRegistry
    
    weak var output: IApplicationInteractorOutput!
    
    init(state: IApplicationState, analyticsRegistry: AnalyticsEventRegistry) {
        
        self.state = state
        self.analyticsRegistry = analyticsRegistry
    }
    
    func start() throws {
        
        state.addListener(self)
        state.changeState(to: .starting)
        
        updateWorkingState()
        
        analyticsRegistry.register(event: .start)
    }
    
    func becomeActive() {
        updateWorkingState()
    }
    
    func enterBackground() {
        state.changeState(to: .background)
        logger.flush()
    }
    
    func enterForeground() {
        
    }
    
    func terminate() {
        state.changeState(to: .terminating)
        logger.flush()
    }
    
    private func updateWorkingState() {
        
        state.changeState(to: .running)
    }
}

extension ApplicationInteractor: IApplicationStateListener {
    
    func stateChanged(to newState: ApplicationState, from previousState: ApplicationState) {
        
        output.applicationStateChanged(to: newState, from: previousState)
    }
}
