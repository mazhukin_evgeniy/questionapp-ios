// Copyright © 2018 SWTecNN. All rights reserved.

import Foundation

protocol IModuleInputReceiver {
    
    var moduleInput: Any? { get set }
}
