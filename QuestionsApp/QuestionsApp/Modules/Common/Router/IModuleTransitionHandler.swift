// Copyright © 2018 SWTecNN. All rights reserved.

import Foundation

protocol IModuleTransitionHandler: class {
    
    typealias TransitionBlockType = (_ moduleInput: Any) -> Void
    func performTransition(_ identifier: String)
    func openModule(id: String, with transition: @escaping TransitionBlockType)
    func close()
}
