// Copyright © 2019 SWTecNN. All rights reserved.

import UIKit

class QuestionTableViewCell: UITableViewCell {
    
    private let buttonsStackViewTopConstraintsDefault: CGFloat = 10
    private let buttonsStackViewHeightConstraintDefault: CGFloat = 44
    private let buttonsStackViewHeightConstraintOneButton: CGFloat = 40

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var voteCountLabel: UILabel!
    @IBOutlet weak var voteImageView: UIImageView!
    
    @IBOutlet weak var buttonsStackView: UIStackView!
    @IBOutlet weak var primaryButton: Button!
    @IBOutlet weak var secondaryButton: Button!
    
    @IBOutlet weak var buttonsStackViewTopConstraints: NSLayoutConstraint!
    @IBOutlet weak var buttonsStackViewHeightConstraint: NSLayoutConstraint!
    
    private var primaryButtonAction: (() -> Void)?
    private var secondaryButtonAction: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        reset()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        reset()
    }
    
    private func reset() {
        questionLabel.textColor = .sectionTitle
        voteCountLabel.textColor = .sectionTitle
        
        hideButtonsStackView()
        primaryButton.isHidden = true
        secondaryButton.isHidden = true
        
        primaryButton.modeValue = .default
        secondaryButton.modeValue = .default
        
        primaryButtonAction = nil
        secondaryButtonAction = nil
    }
    
    private func showButtonsStackView() {
        buttonsStackViewTopConstraints.constant = buttonsStackViewTopConstraintsDefault
        buttonsStackViewHeightConstraint.constant =
            buttonsStackView.isHidden ?
            buttonsStackViewHeightConstraintOneButton :
            buttonsStackViewHeightConstraintDefault
        buttonsStackView.isHidden = false
    }
    
    private func hideButtonsStackView() {
        guard !buttonsStackView.isHidden else { return }
        buttonsStackView.isHidden = true
        buttonsStackViewTopConstraints.constant = 0
        buttonsStackViewHeightConstraint.constant = 0
    }
    
    func setupWithQuestion(_ question: SocialEventQuestion) {
        nameLabel.text = question.displayName
        questionLabel.text = question.description
        voteCountLabel.text = String(question.voteCount)
        
        let voteCountTextColor: UIColor = question.archived ?
            .inactiveColor :
            question.voted ? .defaultButtonTitle : .sectionTitle
        voteCountLabel.textColor = voteCountTextColor
        
        let imageName = question.archived ?
            "like-icon-inactive" :
            question.voted ? "like-icon-active" : "like-icon-unlike"
        voteImageView.image = UIImage(named: imageName)
    }
    
    func setPrimaryButtonTitle(_ title: String, mode: Button.Mode, action: @escaping () -> Void) {
        showButtonsStackView()
        primaryButton.isHidden = false
        primaryButton.setTitle(title, for: .normal)
        primaryButton.modeValue = mode
        primaryButtonAction = action
    }
    
    func setSecondaryButtonTitle(_ title: String, mode: Button.Mode, action: @escaping () -> Void) {
        showButtonsStackView()
        secondaryButton.isHidden = false
        secondaryButton.setTitle(title, for: .normal)
        secondaryButton.modeValue = mode
        secondaryButtonAction = action
    }
    
    @IBAction func primaryButtonPressed(_ sender: UIButton) {
        primaryButtonAction?()
    }
    
    @IBAction func secondaryButtonPressed(_ sender: UIButton) {
        secondaryButtonAction?()
    }
}
