// Copyright © 2019 SWTecNN. All rights reserved.

import UIKit

class SocialViewFactory {
    
    static func getSocialEventView(with id: String, role: Role) -> UIViewController? {
        
        switch role {
        case .admin, .client:
            return getActiveSocialEventView(with: id, role: role)
        case .viewer:
            return getInfoSocialEventView(with: id)
        }
    }
    
    private static func getInfoSocialEventView(with id: String) -> UIViewController? {
        guard let info = UIStoryboard(name: "SocialEventInfo", bundle: nil).instantiateInitialViewController() as? SocialEventInfoViewController,
            let input = info.moduleInput as? ISocialEventInfoModule  else {
            return nil
        }
        
        input.role = .viewer
        input.socialEventId = id
        
        return info
    }

    private static func getActiveSocialEventView(with id: String, role: Role) -> UIViewController? {
        
        guard let socialEvent = UIStoryboard(name: "SocialEvent", bundle: nil).instantiateInitialViewController() as? SocialEventViewController else {
            return nil
        }
        
        switch role {
        case .admin:
            socialEvent.intialize(mode: .admin)
            let input = socialEvent.moduleInput as? ISocialEventAdminModule
            input?.socialEventId = id
        case .client:
            socialEvent.intialize(mode: .client)
            let input = socialEvent.moduleInput as? ISocialEventClientModule
            input?.socialEventId = id
        default: break
        }
        
        return socialEvent
    }
    
}
