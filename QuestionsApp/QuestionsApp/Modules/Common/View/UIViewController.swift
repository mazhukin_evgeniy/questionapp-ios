// Copyright © 2019 SWTecNN. All rights reserved.

import UIKit

extension UIViewController {

    func showErrorMessage(_ message: String?) {

        showAlert(with: lcMsg("error"), and: message)
    }

    func showAlert(with title: String?, and message: String?) {

        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: lcMsg("ok"), style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true)
    }
}
