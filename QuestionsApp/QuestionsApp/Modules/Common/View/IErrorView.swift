// Copyright © 2019 SWTecNN. All rights reserved.

import Foundation

protocol IErrorView {
    
    func handleError(_ error: Error)
}
