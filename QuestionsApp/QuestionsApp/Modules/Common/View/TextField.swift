// Copyright © 2019 SWTecNN. All rights reserved.

import UIKit

class TextField: UITextField {

    private let textXInset = CGFloat(16.0)

    override func textRect(forBounds bounds: CGRect) -> CGRect {

        return bounds.insetBy(dx: textXInset, dy: 0.0)
    }

    override func editingRect(forBounds bounds: CGRect) -> CGRect {

        return bounds.insetBy(dx: textXInset, dy: 0.0)
    }

}
