// Copyright © 2019 SWTecNN. All rights reserved.

import UIKit

class Button: UIButton {
    
    enum Mode: String {
        case light
        case `default`
        case cancel
        case blue
    }
    
    @objc var mode: String? {
        didSet {
            guard let mode = Mode(rawValue: mode ?? "default") else { return }
            modeValue = mode
        }
    }
    
    var modeValue: Mode = .default {
        didSet { updateAppearance() }
    }
    
    func updateAppearance() {
        
        var backgroundColor: UIColor?
        var titleColor: UIColor?
        
        switch modeValue {
        case .cancel:
            backgroundColor = .cancelButtonBackground
            titleColor = .cancelButtonTitle
        case .default:
            backgroundColor = .defaultButtonBackground
            titleColor = .defaultButtonTitle
        case .light:
            backgroundColor = .lightButtonBackground
            titleColor = .lightButtonTitle
        case .blue:
            backgroundColor = .blueButtonBackground
            titleColor = .blueButtonTitle
        }
        
        self.backgroundColor = backgroundColor
        setTitleColor(titleColor, for: .normal)
        layer.cornerRadius = 10
        titleLabel?.font = .buttonTitle
    }
}

