//  Copyright © 2019 SWTecNN. All rights reserved.

class AuthenticationPresenter: IAuthenticationModule {

    weak var view: IAuthenticationView!
    var interactor: IAuthenticationInteractor!
    var router: IAuthenticationRouter!

    private var passwordHash: String?
    private var socialEvent: SocialEvent!
}

extension AuthenticationPresenter: IAuthenticationInteractorOutput {

    func socialEventObtained(_ socialEvent: SocialEvent) {

        self.socialEvent = socialEvent
        
        if socialEvent.archived {
            router.openSocialEventViewer(id: socialEvent.id)
            view.hideProgress()
        } else if passwordHash != nil {
            view.showPassword()
            view.hideProgress()
        } else {
            router.openSocialEventClient(id: socialEvent.id)
            view.dismissAuthentication()
        }
    }

    func socialEventObtainError(_ error: Error) {

        view.hideProgress()
        view.handleError(error)
    }
}

extension AuthenticationPresenter: IAuthenticationViewOutput {

    func viewIsShown() {

        view.startScanning()
    }

    func viewIsHidden() {

        view.stopScanning()
    }
    
    func openSocialEventAdminPressed(id: String) {
        router.openSocialEventAdmin(id: id)
    }

    func qrCodeScanned(_ code: String) {

        if let qrCode = QRCode.create(from: code) {
            view.stopScanning()
            view.showProgress()
            passwordHash = qrCode.passwordHash
            interactor.obtainSocialEvent(with: qrCode.socialEventId)
        }
    }

    func submitPasswordPressed(_ password: String) {

        if interactor.checkPassword(socialEvent.password, code: password, passwordHash: passwordHash ?? "") {
            router.openSocialEventAdmin(id: socialEvent.id)
            view.dismissAuthentication()
        } else {
            view.showIncorrectPasswordError()
        }
    }
}
