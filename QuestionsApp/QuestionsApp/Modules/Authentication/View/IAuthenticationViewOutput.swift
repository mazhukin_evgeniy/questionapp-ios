//  Copyright © 2019 SWTecNN. All rights reserved.

protocol IAuthenticationViewOutput {

    func viewIsShown()
    func viewIsHidden()
    func openSocialEventAdminPressed(id: String)
    func qrCodeScanned(_ code: String)
    func submitPasswordPressed(_ password: String)
}
