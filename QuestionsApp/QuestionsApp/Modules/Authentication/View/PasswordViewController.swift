// Copyright © 2019 SWTecNN. All rights reserved.

import UIKit

class PasswordViewController: UIViewController {

    @IBOutlet weak var passwordTextField: TextField!

    var onResult: ((String) -> Void)?

    @IBAction func submitButtonPressed(_ sender: Any) {

        onResult?(passwordTextField.text ?? "")
    }
}
