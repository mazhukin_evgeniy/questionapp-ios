//  Copyright © 2019 SWTecNN. All rights reserved.

import Foundation

protocol IAuthenticationInteractor {

    func obtainSocialEvent(with id: String)
    func checkPassword(_ password: String, code: String, passwordHash: String) -> Bool
}
