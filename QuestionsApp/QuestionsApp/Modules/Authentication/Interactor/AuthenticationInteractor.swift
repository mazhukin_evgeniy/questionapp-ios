//  Copyright © 2019 SWTecNN. All rights reserved.

class AuthenticationInteractor: IAuthenticationInteractor {

    weak var output: IAuthenticationInteractorOutput!

    let socialEventRepository: SocialEventRepository
    let cipher: ISocialEventCipher

    init(socialEventRepository: SocialEventRepository, cipher: ISocialEventCipher) {

        self.socialEventRepository = socialEventRepository
        self.cipher = cipher
    }

    func obtainSocialEvent(with id: String) {

        async {
            do {
                let socialEvent = try self.socialEventRepository.getSocialEvent(with: id)
                asyncMain {
                    self.output.socialEventObtained(socialEvent)
                }
            } catch {
                asyncMain {
                    self.output.socialEventObtainError(error)
                }
            }
        }
    }

    func checkPassword(_ password: String, code: String, passwordHash: String) -> Bool {

        return password == cipher.decrypt(code: code, hash: passwordHash)
    }
}
