//  Copyright © 2019 SWTecNN. All rights reserved.

import UIKit

class AuthenticationModuleInitializer: NSObject {

    @IBOutlet weak var authenticationViewController: AuthenticationViewController!

    override func awakeFromNib() {

        let assembly = AuthenticationAssembly()
        assembly.assemblyModuleForView(view: authenticationViewController)
    }

}
