//  Copyright © 2019 SWTecNN. All rights reserved.

import UIKit

class AuthenticationAssembly {

    func assemblyModuleForView<UIViewController>(view: UIViewController) {

        if let viewController = view as? AuthenticationViewController {
            assembly(viewController: viewController)
        }
    }

    private func assembly(viewController: AuthenticationViewController) {

        let presenter = AuthenticationPresenter()
        presenter.view = viewController
        presenter.router = viewController

        let socialEventRepository = Service.factory.getSocialEventRepository()
        let socialEventCipher = Service.factory.getSocialEventCipher()

        let interactor = AuthenticationInteractor(socialEventRepository: socialEventRepository, cipher: socialEventCipher)
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
