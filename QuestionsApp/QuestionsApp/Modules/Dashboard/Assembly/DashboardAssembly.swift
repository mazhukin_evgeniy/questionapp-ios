//  Copyright © 2019 SWTecNN. All rights reserved.

import UIKit

class DashboardAssembly {

    func assemblyModuleForView<UIViewController>(view: UIViewController) {

        if let viewController = view as? DashboardViewController {
            assembly(viewController: viewController)
        }
    }

    private func assembly(viewController: DashboardViewController) {

        let presenter = DashboardPresenter()
        presenter.view = viewController
        presenter.router = viewController

        let clientSessionRepository = Service.factory.getClientSessionRepository()
        let clientSocialEventRepository = Service.factory.getClientSocialEventRepository()
        let socialEventRepository = Service.factory.getSocialEventRepository()

        let interactor = DashboardInteractor(clientSessionRepository: clientSessionRepository,
                                             clientSocialEventRepository: clientSocialEventRepository,
                                             socialEventRepository: socialEventRepository)
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }
}
