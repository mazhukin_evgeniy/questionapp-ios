//  Copyright © 2019 SWTecNN. All rights reserved.

import UIKit

class DashboardModuleInitializer: NSObject {

    @IBOutlet weak var dashboardViewController: DashboardViewController!

    override func awakeFromNib() {

        let assembly = DashboardAssembly()
        assembly.assemblyModuleForView(view: dashboardViewController)
    }
}
