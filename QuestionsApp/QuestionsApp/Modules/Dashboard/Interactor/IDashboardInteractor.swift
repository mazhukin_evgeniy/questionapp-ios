//  Copyright © 2019 SWTecNN. All rights reserved.

import Foundation

protocol IDashboardInteractor {

    func obtainName()
    func saveName(_ name: String)
    func obtainSocialEvents()
    func obtainSocialEvent(with socialEventId: String)
}
