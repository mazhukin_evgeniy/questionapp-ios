//  Copyright © 2019 SWTecNN. All rights reserved.

class DashboardPresenter: IDashboardModule {

    private let maxSocialEventsToShow = 2

    weak var view: IDashboardView!
    var interactor: IDashboardInteractor!
    var router: IDashboardRouter!
}

extension DashboardPresenter: IDashboardInteractorOutput {

    func nameObtained(_ name: String) {

        view?.showName(name)
    }

    func nameObtainError(_ error: Error) {

        view.handleError(error)
    }

    func nameSaved() {

        interactor.obtainName()
    }

    func nameSaveError(_ error: Error) {

        view.handleError(error)
    }

    func socialEventsObtained(_ socialEvents: [SocialEventModel]) {

        view.setSocialEvents(socialEvents)

        let socialEventsToShow = Array(socialEvents.prefix(maxSocialEventsToShow))
        view.showSocialEvents(socialEventsToShow)
        view.hideProgress()
    }
    
    func socialEventObtained(_ socialEvent: SocialEvent) {
        
        if socialEvent.archived {
            router.openSocialEventInfo(with: socialEvent.id, role: .viewer)
        } else {
            router.openSocialEvent(with: socialEvent.id, role: .client)
        }
        view.hideProgress()
    }
    
    func socialEventsObtainError(_ error: Error) {

        view.hideProgress()
        view.handleError(error)
    }
    
    func socialEventObtainError(_ error: Error) {
        view.hideProgress()
        view.handleError(error)
    }
}

extension DashboardPresenter: IDashboardViewOutput {

    func viewIsReady() {
        view.showProgress()
        interactor.obtainName()
        interactor.obtainSocialEvents()
    }

    func openSocialEventInfoPressed(with id: String) {
        view.showProgress()
        interactor.obtainSocialEvent(with: id)
    }

    func saveNamePressed(_ name: String) {

        interactor.saveName(name)
    }

    func scanPressed() {

        router.openAuthentication()
    }
}
