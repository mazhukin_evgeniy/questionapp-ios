//  Copyright © 2019 SWTecNN. All rights reserved.

protocol IDashboardViewOutput {

    func viewIsReady()
    func openSocialEventInfoPressed(with id: String)
    func saveNamePressed(_ name: String)
    func scanPressed()
}
