// Copyright © 2019 SWTecNN. All rights reserved.

import UIKit

class SocialEventTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var roleLabel: UILabel?

    func setSocialEvent(_ socialEventModel: SocialEventModel) {

        titleLabel.text = socialEventModel.socialEvent.title

        let formatter = DateFormatter(format: "dd.MM.YYYY HH:mm")
        dateLabel.text = formatter.string(from: socialEventModel.socialEvent.date)

        roleLabel?.text = lcMsg(socialEventModel.role.rawValue)
    }
}
