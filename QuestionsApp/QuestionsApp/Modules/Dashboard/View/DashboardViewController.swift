//  Copyright © 2019 SWTecNN. All rights reserved.

import UIKit

class DashboardViewController: BaseViewController {

    struct SegueIdentifier {

        static let showSocialEvents = "showSocialEvents"
    }

    private let socialEventCellId = "SocialEventCell"

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var seeAllButton: Button!
    @IBOutlet weak var socialEventsHeaderLabel: UILabel!

    var output: IDashboardViewOutput!

    private var name: String?
    private var socialEvents = [SocialEventModel]()
    private var socialEventsToShow = [SocialEventModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupTableView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        output.viewIsReady()
    }

    private func setupTableView() {

        tableView.dataSource = self
        tableView.delegate = self
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)

        switch segue.identifier {
        case SegueIdentifier.showSocialEvents:
            guard let socialEventsView = segue.destination as? SocialEventTableViewController else {
                break
            }

            socialEventsView.socialEvents = socialEvents
            socialEventsView.onSocialEventSelected = { [unowned self] socialEvent in
                self.output.openSocialEventInfoPressed(with: socialEvent.id)
            }

        default:
            break;
        }
    }

    private func showEditNameView() {

        let alert = UIAlertController(title: lcMsg("your_name"), message: nil, preferredStyle: .alert)
        alert.setTitleFont(.alertTitle, color: .black)
        alert.addTextField { [unowned self] textField in

            textField.autocapitalizationType = .words
            textField.placeholder = lcMsg("default_name")
            textField.text = self.name
        }

        let saveAction = UIAlertAction(title: lcMsg("name_save"), style: .default) { [unowned self] action in

            if let name = alert.textFields?.first?.text {
                self.output.saveNamePressed(name)
            }
        }
        alert.addAction(saveAction)

        let closeAction = UIAlertAction(title: lcMsg("name_close"), style: .cancel, handler: nil)
        alert.addAction(closeAction)

        present(alert, animated: true)
    }

    @IBAction func editNameButtonPressed(_ sender: Any) {

        showEditNameView()
    }

    @IBAction func scanButtonPressed(_ sender: Any) {

        output.scanPressed()
    }

    @IBAction func seeAllButtonPressed(_ sender: Any) {

        performSegue(withIdentifier: SegueIdentifier.showSocialEvents, sender: nil)
    }
}

extension DashboardViewController: IDashboardView {

    func showName(_ name: String) {
        self.name = name
        nameLabel.text = name
    }

    func setSocialEvents(_ socialEvents: [SocialEventModel]) {

        self.socialEvents = socialEvents
    }

    func showSocialEvents(_ socialEvents: [SocialEventModel]) {

        socialEventsToShow = socialEvents
        socialEventsHeaderLabel.isHidden = socialEvents.isEmpty
        seeAllButton.isHidden = socialEvents.isEmpty
        tableView.reloadData()
    }
}

extension DashboardViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return socialEventsToShow.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = tableView.dequeueReusableCell(withIdentifier: socialEventCellId, for: indexPath) as? SocialEventTableViewCell else {
            return UITableViewCell()
        }

        cell.setSocialEvent(socialEventsToShow[indexPath.row])

        return cell
    }
}

extension DashboardViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        output.openSocialEventInfoPressed(with: socialEventsToShow[indexPath.row].socialEvent.id)
    }
}

extension DashboardViewController: IDashboardRouter {
    
    func openSocialEventInfo(with id: String, role: Role) {

        if let info = SocialViewFactory.getSocialEventView(with: id, role: role) {
            
            navigationController?.pushViewController(info, animated: true)
        }
    }
    
    func openSocialEvent(with id: String, role: Role) {
        if let event = SocialViewFactory.getSocialEventView(with: id, role: role) {
            
            navigationController?.pushViewController(event, animated: true)
        }
    }

    func openAuthentication() {

        performTransition("showAuthentication")
    }
}
