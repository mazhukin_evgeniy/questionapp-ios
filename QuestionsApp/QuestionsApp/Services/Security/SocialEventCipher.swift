// Copyright © 2019 SWTecNN. All rights reserved.

import Foundation
import CryptoSwift

public class SocialEventCipher: ISocialEventCipher {
    
    private let key = "812d3dc807cfd8ec"
    private let logger = Logger.factory.getLogger(for: .default)
    
    public func encrypt(code: String) -> (code: String, hash: String) {
        
        return encrypt(code: code, iv: AES.randomIV(AES.blockSize))
    }
    
    public func decrypt(code: String, hash: String) -> String {
        
        return encrypt(code: code, iv: Array(hex: hash)).code
    }
    
    private func encrypt(code: String, iv: [UInt8]) -> (code: String, hash: String) {
        
        let input: Array<UInt8> = code.bytes
        do {
            let encrypted = try AES(key: key.bytes, blockMode: CBC(iv: iv), padding: .pkcs7).encrypt(input)
            return (code: encrypted.toHexString(), hash: iv.toHexString())
        } catch {
            logger.error("Unable to generate code", error: error)
            return (code: "", hash: "")
        }
    }
}
