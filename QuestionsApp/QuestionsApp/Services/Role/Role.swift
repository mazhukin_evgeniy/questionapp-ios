// Copyright © 2019 SWTecNN. All rights reserved.

enum Role {
    case admin
    case client
    case viewer
}
