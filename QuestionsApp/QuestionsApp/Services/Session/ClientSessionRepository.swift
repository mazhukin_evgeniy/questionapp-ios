// Copyright © 2019 SWTecNN. All rights reserved.

import Foundation

public protocol ClientSessionRepository {

    func saveName(_ name: String) throws
    func readName() throws -> String
}
