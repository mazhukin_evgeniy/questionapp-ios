// Copyright © 2018 SWTecNN. All rights reserved.

import Foundation

public protocol DeviceInfoProvider {
    
    func getDeviceId() -> String
    func getOSVersion() -> String
    func getLocale() -> String
    func getApplicationVersion() -> String
    func getPushToken() throws -> String
}
