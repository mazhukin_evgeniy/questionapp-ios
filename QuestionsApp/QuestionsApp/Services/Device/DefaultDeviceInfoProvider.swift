// Copyright © 2018 SWTecNN. All rights reserved.

import Foundation
import UIKit

public class DefaultDeviceInfoProvider: DeviceInfoProvider {
    
    private let tokenProvider: ITokenProvider
    
    init(tokenProvider: ITokenProvider) {
        self.tokenProvider = tokenProvider
    }
    
    public func getDeviceId() -> String {
        return UIDevice.current.identifierForVendor?.uuidString ?? "null"
    }
    
    public func getOSVersion() -> String {
        let osVersion = UIDevice.current.systemVersion
        return osVersion
    }
    
    public func getApplicationVersion() -> String {
        
        let appVersionString = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        let buildNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
        
        return "\(appVersionString).\(buildNumber)"
    }
    
    public func getLocale() -> String {
        let locale = NSLocale.current.identifier
        return locale
    }
    
    public func getPushToken() throws -> String {
        return tokenProvider.token ?? ""
    }
}
