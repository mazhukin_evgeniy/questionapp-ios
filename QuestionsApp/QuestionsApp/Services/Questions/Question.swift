// Copyright © 2019 SWTecNN. All rights reserved.

import Firebase

public struct Question: Codable {

    let id: String?
    let name: String?
    let slideNum: Int?
    let socialEventId: String
    let description: String
    var votes: Int
    let archived: Bool
}

extension Question {

    func asFirebaseModel() -> [String: Any] {

        guard let data = try? JSONEncoder().encode(self),
            let dictionary = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
            return [:]
        }

        return dictionary
    }

    static func create(from object: Any, with id: String) -> Question? {

        guard var dictionary = object as? [String: Any] else {
            return nil
        }

        dictionary[CodingKeys.id.stringValue] = id

        guard let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted) else {
            return nil
        }

        return try? JSONDecoder().decode(Question.self, from: data)
    }

    static func createArray(from snapshot: DataSnapshot) -> [Question]? {

        guard let snapshotDict = snapshot.value as? [String: Any] else {
            return nil
        }

        return snapshotDict.compactMap({ Question.create(from: $1, with: $0) })
    }
}

extension Question {

    static var archivedKey: String {

        return CodingKeys.archived.stringValue
    }
}
