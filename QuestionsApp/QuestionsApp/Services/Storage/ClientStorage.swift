// Copyright © 2019 SWTecNN. All rights reserved.

import Foundation

class ClientStorage {
    
    private struct Keys {
        static let questions = "ClientStorage.Questions"
        static let votedQuestions = "ClientStorage.VotedQuestions"
        static let name = "ClientStorage.Name"
        static let raiseHandRequests = "ClientStorage.RaiseHandRequests"
        static let socialEvents = "ClientStorage.SocialEvents"
    }
    
    private let storage: IKeyValueStorage
    
    init(storage: IKeyValueStorage) {
        self.storage = storage
    }
}

extension ClientStorage: ClientQuestionRepository {
    
    func addQuestion(id: String) throws {
        let storedQuestions = try getQuestions()
        var questions = Set(storedQuestions)
        questions.insert(id)
        try storage.set(value: Array(questions), for: Keys.questions)
    }
    
    func getQuestions() throws -> [String] {
        if let questions = try storage.getValue(for: Keys.questions) as? [String] {
            return questions
        }
        return []
    }
    
    func deleteQuestion(with id: String) throws {
        
        var questions = try getQuestions()
        if let index = questions.firstIndex(of: id) {
            questions.remove(at: index)
            try storage.set(value: questions, for: Keys.questions)
        }
        
        var votedQuestions = try getVotedQuestions()
        if let index = votedQuestions.firstIndex(of: id) {
            votedQuestions.remove(at: index)
            try storage.set(value: votedQuestions, for: Keys.votedQuestions)
        }
    }
    
    func updateVote(for questionId: String) throws {
        var questions = try getVotedQuestions()
        if let index = questions.firstIndex(of: questionId) {
            questions.remove(at: index)
        } else {
            questions.append(questionId)
        }
        try storage.set(value: questions, for: Keys.votedQuestions)
    }
    
    func getVotedQuestions() throws -> [String] {
        if let questions = try storage.getValue(for: Keys.votedQuestions) as? [String] {
            return questions
        }
        return []
    }
}

extension ClientStorage: ClientSessionRepository {

    func saveName(_ name: String) throws {

        try storage.set(value: name, for: Keys.name)
    }

    func readName() throws -> String {

        return try storage.getValue(for: Keys.name) as? String ?? lcMsg("default_name")
    }
}

extension ClientStorage: ClientSocialEventRepository {

    func getArchivedSocialEvents() throws -> [ClientSocialEvent] {

        return try getSocialEvents().compactMap({ ClientSocialEvent.create(from: $0) })
    }
    
    func archive(socialEvent: ClientSocialEvent) throws {

        let storedEvents = try getSocialEvents()
        var events = Set(storedEvents)
        if let string = socialEvent.asJsonString() {
            events.insert(string)
        }
        try storage.set(value: Array(events), for: Keys.socialEvents)
    }

    private func getSocialEvents() throws -> [String] {

        if let events = try storage.getValue(for: Keys.socialEvents) as? [String] {
            return Array(Set(events))
        }
        return []
    }

    func addRaiseHandRequest(with id: String, for socialEventId: String) throws {

        var requests = try getRaiseHandRequests()
        requests[socialEventId] = id
        try storage.set(value: requests, for: Keys.raiseHandRequests)
    }

    func getRaiseHandRequestId(for socialEventId: String) throws -> String? {

        let requests = try getRaiseHandRequests()
        return requests[socialEventId]
    }

    func deleteRaiseHandRequest(for socialEventId: String) throws {

        var requests = try getRaiseHandRequests()
        requests.removeValue(forKey: socialEventId)
        try storage.set(value: requests, for: Keys.raiseHandRequests)
    }

    private func getRaiseHandRequests() throws -> [String: String] {

        if let requests = try storage.getValue(for: Keys.raiseHandRequests) as? [String: String] {
            return requests
        }
        return [:]
    }

}
