// Copyright © 2018 SWTecNN. All rights reserved.

import Foundation

public protocol IKeyValueStorage {
    
    func getValue(for key: String) throws -> Any?
    func set(value: Any, for key: String) throws
    func remove(for key: String) throws
}
