// Copyright © 2019 SWTecNN. All rights reserved.

import Foundation

public class UserDefaultsStorage: IKeyValueStorage {
    
    private let storage: UserDefaults
    
    public init() {
        
        storage = UserDefaults.standard
    }
    
    public func getValue(for key: String) throws -> Any? {
        guard let value = storage.value(forKey: key) else {
            return nil
        }
        return value
    }
    
    public func set<T>(value: T, for key: String) throws {
        
        storage.setValue(value, forKey: key)
        storage.synchronize()
    }
    
    public func remove(for key: String) throws {
        storage.removeObject(forKey: key)
        storage.synchronize()
    }
}
