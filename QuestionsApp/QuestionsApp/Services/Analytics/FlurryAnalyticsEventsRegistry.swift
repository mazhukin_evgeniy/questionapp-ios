// Copyright © 2018 SWTecNN. All rights reserved.

import Foundation
import Flurry_iOS_SDK

public class FlurryAnalyticsEventsRegistry: NSObject, AnalyticsEventRegistry {
    
    init(key: String, deviceInfo: DeviceInfoProvider) {
        
        super.init()
        
        let builder = FlurrySessionBuilder()
            .withAppVersion(deviceInfo.getApplicationVersion())
            .withLogLevel(FlurryLogLevelNone)
            .withCrashReporting(true)
            .withSessionContinueSeconds(10)
            .withIncludeBackgroundSessions(inMetrics: true)
        
        Flurry.startSession(key, with: builder)
        Flurry.setUserID(deviceInfo.getDeviceId())
    }
    
    public func register(event: AnalyticsEvent) {
        
        switch event {
        case .start:
            Flurry.logEvent(event.stringRepresentation())
        }
    }
}

extension AnalyticsEvent {
    
    func stringRepresentation() -> String {
        switch self {
        case .start:
            return "start"
        }
    }
}
