// Copyright © 2019 SWTecNN. All rights reserved.

import Firebase

class FirebaseSocialEventRepository: SocialEventRepository {

    private let timeout = TimeInterval(30)

    private let databaseReference: DatabaseReference

    init(reference: DatabaseReference) {

        databaseReference = reference
    }

    func getSocialEvent(with id: String) throws -> SocialEvent {

        var result: SocialEvent?
        let semaphore = BinarySemaphore()
        var err: SocialEventRepositoryError?

        databaseReference.child(FirebasePaths.socialEvents).child(id).observeSingleEvent(of: .value, with: { (snapshot) in

            if let socialEvent = SocialEvent.create(from: snapshot, with: id) {
                result = socialEvent
            } else {
                err = .noSocialEventForId
            }
            semaphore.signal()
        }) { (error) in
            err = .noSocialEventForId
        }

        if semaphore.wait(timeout: timeout) {
            if let result = result {
                return result
            } else {
                throw err ?? .noSocialEventForId
            }
        } else {
            throw SocialEventRepositoryError.timeout
        }
    }

    func archiveSocialEvent(with id: String) throws {

        let semaphore = BinarySemaphore()
        var err: SocialEventRepositoryError?

        databaseReference.child(FirebasePaths.socialEvents).child(id).updateChildValues([SocialEvent.archivedKey: true]) { (error, reference) in

            if error != nil {
                err = .unableToArchive
            }

            semaphore.signal()
        }

        if !semaphore.wait(timeout: timeout) {
            err = .timeout
        }

        if let error = err {
            throw error
        }
    }
}
