This project is dedicated to quick start of iOS projects

To start the project:
1. Copy QuestionsApp folder
2. Install [required software](RequiredSoftware.md)
3. Check how to [use](https://github.com/rambler-digital-solutions/Generamba#usage) generamba:\
   <code>generamba template install\
   generamba gen **ComponentName** swifty_viper</code>
4. [Rename project](RenameProject.md)