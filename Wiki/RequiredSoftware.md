1. Install Xcode
2. [Install Cocoapods](https://guides.cocoapods.org/using/getting-started.html)\
    ```gem install cocoapods```
3. [Install Generamba](https://github.com/rambler-digital-solutions/Generamba#installation)\
    ```gem install generamba```